/**
 * GNU GENERAL PUBLIC LICENSE See the file Launcher.java for copying conditions.
 * 
 * @author mjwhitta
 * @created Jan 6, 2013
 */

package sites.mjwhitta.jiggler.gui;

import java.awt.AWTException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JPanel;

/**
 * This class prevents the computer from going to sleep
 */
public class Jiggler extends JPanel {
   private class MouseJiggler implements Runnable {
      private boolean m_isRunning = true;

      /**
       * @see java.lang.Runnable#run()
       */
      @Override
      public void run() {
         while (m_isRunning) {
            Point p = MouseInfo.getPointerInfo().getLocation();
            if (p.x > 0) {
               m_robot.mouseMove(p.x - 1, p.y);
               m_robot.mouseMove(p.x, p.y);
            } else {
               m_robot.mouseMove(p.x + 1, p.y);
               m_robot.mouseMove(p.x, p.y);
            }
            for (int i = 0; i < 9; ++i) {
               if (m_isRunning) {
                  m_robot.delay(SECONDS_PER_MINUTE * MILLIS_PER_SECOND);
               } else {
                  break;
               }
            }
         }
      }

      /**
       * Stop the thread
       */
      public void stop() {
         m_isRunning = false;
      }
   }

   private static final long serialVersionUID = 1L;

   private static int MILLIS_PER_SECOND = 1000;

   private static int SECONDS_PER_MINUTE = 60;

   private static Jiggler INSTANCE;

   public static Jiggler getInstance() {
      if (INSTANCE == null) {
         INSTANCE = new Jiggler();
      }
      return INSTANCE;
   }

   private MouseJiggler m_mouseJiggler;

   private Thread m_jigglerThread;

   private Robot m_robot;

   private JButton m_start, m_stop;

   private Jiggler() {
      init();
      buildPanel();
   }

   private void buildPanel() {
      GridBagConstraints gbc = new GridBagConstraints();
      gbc.fill = GridBagConstraints.BOTH;
      gbc.insets = new Insets(5, 5, 0, 0);
      gbc.weightx = 1;
      gbc.weighty = 1;
      gbc.gridx = 0;
      gbc.gridy = 0;
      add(m_start, gbc);

      gbc.gridy++;
      add(m_stop, gbc);
   }

   public JMenu[] getMenus() {
      return new JMenu[0];
   }

   private void init() {
      setLayout(new GridBagLayout());

      m_robot = null;
      try {
         m_robot = new Robot();
      } catch (AWTException e) {
         e.printStackTrace();
      }

      m_start = new JButton("Start");
      m_start.addActionListener(new ActionListener() {
         /**
          * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
          */
         @Override
         public void actionPerformed(ActionEvent e) {
            m_start.setEnabled(false);
            m_stop.setEnabled(true);

            m_mouseJiggler = new MouseJiggler();
            m_jigglerThread = new Thread(m_mouseJiggler);
            m_jigglerThread.start();
         }
      });

      m_stop = new JButton("Stop");
      m_stop.addActionListener(new ActionListener() {
         /**
          * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
          */
         @Override
         public void actionPerformed(ActionEvent e) {
            m_start.setEnabled(true);
            m_stop.setEnabled(false);

            m_mouseJiggler.stop();
         }
      });
      m_stop.setEnabled(false);
   }
}
